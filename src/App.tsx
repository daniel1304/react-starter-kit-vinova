import PreviewTheme from 'components/preview-theme/PreviewTheme';
import React from 'react';
import Header from 'components/common/Header'
import Footer from 'components/common/Footer';
import PageLayout from 'components/layout/PageLayout';
import { Box } from '@mui/system';

function App() {
  return (
    <PageLayout title="Preview Theme">
      <Header />
      <Box py={3}>
        <PreviewTheme />
      </Box>
      <Footer />
    </PageLayout>
  );
}

export default App;
