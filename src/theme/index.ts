import { PaletteMode } from "@mui/material";
import { createTheme } from '@mui/material/styles';
import { components } from './components';
export * from './globalStyles';

/**
 * Customized Material UI themes for "light" and "dark" modes.
 * @see https://mui.com/customization/default-theme/
 * 
 * A tool to help design and customize themes for the MUI component library
 * @see https://bareynol.github.io/mui-theme-creator/
 */
const theme = createTheme({
  palette: {
    mode: 'light' as PaletteMode,
    primary: {
      main: '#1976d2',
    },
    secondary: {
      main: '#9c27b0',
    },
  },

  typography: {
    fontFamily: [
      `"Roboto"`,
      `"Helvetica"`,
      `"Arial"`,
      `sans-serif`,
    ].join(","),

    h1: { fontSize: "6rem" },
    h2: { fontSize: "3.75rem" },
    h3: { fontSize: "3rem" },
    h4: { fontSize: "2.125rem" },
    h5: { fontSize: "1.5rem" },
    h6: { fontSize: "1.25rem" },
    button: { textTransform: "none" },
  },
  
  components,
  }
);

export default theme;