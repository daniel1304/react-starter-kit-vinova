import { GlobalStyles as MuiGlobalStyles } from '@mui/material';

export const GlobalStyles = () => (
  <MuiGlobalStyles 
    styles={{
      ul: { margin: 0, padding: 0 }
    }} 
  />
);
