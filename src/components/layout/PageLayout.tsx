import { Stack } from '@mui/material';
import React, { useEffect } from 'react';

interface PageLayoutProps {
	title: string;
	children: React.ReactNode;
}

function PageLayout({ title, children }: PageLayoutProps) {
	useEffect(() => {
		document.title = `${title} | React App`;
		window.scrollTo(0, 0);
	}, [title]);

	return <Stack minHeight="100vh">{children}</Stack>;
}

export default PageLayout;
