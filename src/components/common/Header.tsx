import { AppBar, Box, Button, Container, Toolbar, Typography } from '@mui/material';

export default function Header() {
	return (
		<Box sx={{ flexGrow: 1 }}>
			<AppBar position="static" elevation={0}>
				<Toolbar component={Container}>
					<Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
						React Starter Kit - Vinova
					</Typography>
					<Button color="inherit">Login</Button>
				</Toolbar>
			</AppBar>
		</Box>
	);
}
