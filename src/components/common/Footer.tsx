import { Container, Typography } from '@mui/material';
import { Box } from '@mui/system';

function Footer() {
	return (
		<Box
			component="footer"
			sx={{
				py: 2,
				backgroundColor: (theme) => theme.palette.grey[200],
			}}
		>
			<Container>
				<Typography variant="body1">Footer - Copyright © {new Date().getFullYear()}</Typography>
			</Container>
		</Box>
	);
}

export default Footer;
