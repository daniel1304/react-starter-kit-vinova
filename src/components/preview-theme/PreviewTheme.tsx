import { Box, Container, Divider, Typography } from '@mui/material';
import PreviewButton from './Button';
import PreviewTextField from './TextField';
import PreviewTypography from './Typography';

function PreviewTheme() {
	return (
		<Container>
			<Box mb={5}>
				<Typography variant="h3" gutterBottom>
					Button
				</Typography>
				<Divider />
				<PreviewButton />
			</Box>
			<Box mb={5}>
				<Typography variant="h3" gutterBottom>
					TextField
				</Typography>
				<Divider />
				<PreviewTextField />
			</Box>
			<Box>
				<Typography variant="h3" gutterBottom>
					Typography
				</Typography>
				<Divider />
				<PreviewTypography />
			</Box>
		</Container>
	);
}

export default PreviewTheme;
